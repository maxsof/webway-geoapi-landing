import { Swiper, Navigation } from "swiper/js/swiper.esm.js";

Swiper.use([ Navigation ]);

document.querySelectorAll(".swiper-container").forEach((el) => {
  new Swiper(el, {
    loop: true,
    spaceBetween: 60,
    mousewheel: {
      forceToAxis: true,
      invert: true,
    },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });
});
